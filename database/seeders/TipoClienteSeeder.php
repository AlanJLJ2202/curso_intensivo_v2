<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TipoCliente;

class TipoClienteSeeder extends Seeder
{
    
    public function run()
    {
        //Para hacer 20 registros de clientes

        /*for ($i=1; $i < 21; $i++) { 
            
            $tipo_cliente = new TipoCliente;
            $tipo_cliente->nombre = "Cliente $i";
            $tipo_cliente->save();        
        }*/

        $tipos = ['prospecto', 'cliente'];

        foreach ($tipos as $value) {
            
            $tipo_cliente = new TipoCliente;
            $tipo_cliente->nombre = $value;
            $tipo_cliente->save();     
        }
        
    }
}
