<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Cliente extends Model
{
    
    protected $table = 'clientes';
    //Sirven para agregarle columnas a la tabla
    protected $appends = ['tipo', 'ciudad'];

    //Esto se escribe en camel case obligatorio para que la identifique

    public function getTipoAttribute()
    {
        return $this->tipo_cliente->nombre;
    }

    public function getCiudadAttribute()
    {
        return 'Purísima';
    }

    /*public function calificaciones()
    {
        //recibe 3 parametros, la tabla donde se relacionara, la llave foranea, y el id del modelo actual que por defecto = id
        return $this->hasMany(Calificacion::class, 'estudiante_id', 'id');
    }*/

    public function tipo_cliente()
    {
        return $this->belongsTo(TipoCliente::class, 'tipo_id');
    }

    public function telefonos()
    {
        return $this->hasMany(ClienteTelefono::class, 'cliente_id');
    }

    public function correos()
    {
        return $this->hasMany(ClienteCorreo::class, 'cliente_id');
    }

}
