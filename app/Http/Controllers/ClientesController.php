<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\ClienteTelefono;
use App\Models\ClienteCorreo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientesController extends Controller
{
    

    public function store(Request $request)
    {

        //Para empezar una transaccion

        DB::beginTransaction();


        try
        {


            $cliente = new Cliente;
            $cliente->nombres = $request->input('nombres');
            $cliente->apellidos = $request->input('apellidos');
            $cliente->empresa = $request->input('empresa');
            $cliente->tipo_id = $request->input('tipo_id');
            $cliente->save();
    
            $telefonos = $request->input('telefonos');
    
            foreach ($telefonos as $value) {
                
                $telefono = new ClienteTelefono;
                $telefono->telefono = $value['telefono'];
                $telefono->tipo = $value['tipo'];
                $telefono->cliente_id = $cliente->id;
                $telefono->save();
    
            }
    
    
            $correos = $request->input('correos');
    
            foreach ($correos as $value) {
                
                $correo = new ClienteCorreo;
                $correo->email = $value['email'];
                $correo->tipo = $value['tipo'];
                $correo->cliente_id = $cliente->id;
                $correo->save();
    
            }
    
            DB::commit();

            $cliente = Cliente::with('tipo_cliente','telefonos', 'correos')->find($cliente->id);

            return response()->json($cliente);

        
        }catch(\Exception $e)
        {

            DB::rollback();

            $error = [
                'error' => 'Ocurrio un error',
                'mensaje' => $e->getMessage(),
                'linea' => $e->getLine(),
                'trace' => $e->getTrace()
            ];

            \Log::info('Error al agregar un cliente y sus datos', $error);

            return response()->json($error, 500);

        }



       

    }

    public function index()
    {
        $clientes = Cliente::with('telefonos', 'correos')->get();
        return response()->json($clientes);
    }

    
}
